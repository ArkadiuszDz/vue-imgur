import Vue from 'vue'
import Router from 'vue-router'
import ImageList from '@/components/ImageList'
import AuthHandler from '@/components/AuthHandler'
import UploadForm from '@/components/UploadForm'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'ImageList',
      component: ImageList
    },
    {
      path: '/upload',
      component: UploadForm
    },
    {
      path: '/oauth2/callback',
      component: AuthHandler
    }
  ]
})
