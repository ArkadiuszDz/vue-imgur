import api from '../../api/imgur'
import router from '@/router'

const state = {
  images: []
}

const getters = {
  allImages () {
    return state.images
  }
}
// rootState gives the ability to reach into other modules and access state or data that is held inside them
const actions = {
  async fetchImages ({ rootState, commit }) {
    const response = await api.fetchImages(rootState.auth.token)
    commit('setImages', response.data.data)
  },
  async uploadImages ({ rootState }, images) {
    // Get the access token
    const { token } = rootState.auth
    // Call our API module to do the upload
    await api.uploadImages(images, token)
    // Redirect our user to ImageList component
    router.push('/')
  }
}

const mutations = {
  setImages (state, imagesList) {
    state.images = imagesList
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
